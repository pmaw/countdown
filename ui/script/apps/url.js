$(document).ready(() => {
    let timer = $('div#timer');

    $('div#stats').hide();
    $('div#time_set').hide();
    $('div#timer').hide();

    $('button#set').prop('disabled', true);
    
    $('form').submit((e) => {
        e.preventDefault();
        let url = `http://${$('input#remote-url').val()}`;
        let origURL = new URL(url), callURL = new URL('http://localhost:8181');

        callURL.protocol = origURL.protocol;
        callURL.host = origURL.host;
        
        if (url.split(":").length == 2) {
            callURL.port = 8181;
        } else {
            callURL.port = origURL.port;
        }

        var xhr = new XMLHttpRequest;
        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                $('div#stats').fadeOut(300);
                if (xhr.responseText === '') {
                    openMain(false, 0, callURL.toString());
                } else {
                    openMain(true, parseInt(xhr.responseText), callURL.toString());
                }
            }
        }
        xhr.onerror = () => {
            $('div#stats').html('Check IP or Server and try again.');
            $('div#stats').fadeIn(300, () => {
                setTimeout(() => {
                    $('div#stats').fadeOut(300);
                }, 5000);
            });
        }

        xhr.open('GET', callURL.toString(), true);
        try {
            xhr.send();
        } catch (e) {
            $('div#stats').html(e);
        }
    });
});