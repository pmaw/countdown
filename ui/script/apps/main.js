const updateInterval = 10000;

var openMain = (is_set, time, host) => {
    $('div#remote').fadeOut(500, () => {
        $('div#timer').fadeIn(500, main(is_set, time, host));
    });
}

let beginDateOp = (host, t, cb) => {
    let req = new XMLHttpRequest;
    
    req.onreadystatechange = () => {
        if (req.readyState === 4) {
            switch (req.status) {
                case 200:
                    // Success SET_TIME
                    if (req.response == null || req.response == '') {
                        cb(undefined);
                        break;
                    }
                    cb(parseInt(req.response));
                    break;
                    
                // Error SET_TIME
                case 410:
                    $('div#stats').html(`Check you time. (Minimum = ${setMinSec}s)`);
                    $('div#stats').fadeIn(300, () => {
                        setTimeout(() => {
                            $('div#stats').fadeOut(300);
                        }, 5000);
                    });
                    cb(false);
                    break;
                case 401:
                    $('div#stats').html('Check you permission with server.');
                    $('div#stats').fadeIn(300, () => {
                        setTimeout(() => {
                            $('div#stats').fadeOut(300);
                        }, 5000);
                    });
                    cb(false);
                    break;
                default:
                    cb(false);
            }
        }
    };
    if (t !== null) {
        req.open('get', `${host}?t=${t}`, true);
    } else {
        req.open('get', host, true);
    }
    req.send();
}

// Render the array
let render = (x, e) => {
    var i = 0;
    x.forEach(y => {
        x[i] = (y > 9) ? y.toString() : `0${y}`;
        i += 1;
    });

    var i = 0;
    e.forEach(y => {
        if (y.html() == x[i]) {
            i += 1;
            return;
        }
        y.html(x[i]);
        i += 1;
    });
}

let deltaTimestampToArray = (x, z) => {
    let y = [], dt = new Date(x - z);
    y[0] = dt.getUTCFullYear() - 1970;
    y[1] = dt.getUTCMonth();
    y[2] = dt.getUTCDate() - 1;
    y[3] = dt.getUTCHours();
    y[4] = dt.getUTCMinutes();
    y[5] = dt.getUTCSeconds();
    return y;
}

let timestampToArray = (x) => {
    return deltaTimestampToArray(x, 0);
}

var main = (is_set, time, host) => {
    const setMinSec = 10;
    let targetTime = 0, timeElements = [$('a.time#y'), $('a.time#m'), $('a.time#d'), $('a.time#h'), $('a.time#min'), $('a.time#s')];
    let auto_update = false, firstSet = false, isToolOpen = false;

    render([0, 0, 0, 0, 0, 0], timeElements);
    beginDateOp(host, null, (d) => {
        if (d === undefined || d === false) {
            var evt = new CustomEvent('showTool');
            window.dispatchEvent(evt);
        } else {
            var evt = new CustomEvent('hideTool');
            targetTime = d;
            render(deltaTimestampToArray(targetTime, (new Date())), timeElements);
            firstSet = true;
            auto_update = true;
        }
    });

    window.addEventListener('toggleTool', () => {
        if ($('div#time_set').is(':visible')) {
            var evt = new CustomEvent('hideTool');
            window.dispatchEvent(evt);
        } else {
            var evt = new CustomEvent('showTool');
            window.dispatchEvent(evt);
        }
    });
    window.addEventListener('showTool', () => {
        isToolOpen = true;
        $('div#time_set').slideDown(200, () => {
            render([0, 0, 0, 0, 0, 0], timeElements);
            $('button#set').prop('disabled', false);
        });
    });
    window.addEventListener('hideTool', () => {
        isToolOpen = false;
        $('button#set').prop('disabled', true);
        $('div#time_set').slideUp(200);
    });

    $('div#time_set > button, div#tool > button').click((e) => {
        let dateSet = []
        var i = 0;
        timeElements.forEach(element => {
            dateSet[i] = parseInt(element.html());
            i += 1;
        });

        // Time change
        switch ($(e.target).attr('id')) {
            case 'y+':
                if (dateSet[0] < 99) dateSet[0] += 1;
                break;
            case 'y-':
                if (dateSet[0] > 0) {
                    dateSet[0] -= 1;
                }
                if (dateSet[0] === 0) {
                    dateSet[0] = 99;
                }
                break;

            case 'm+':
                if (dateSet[1] < 12) dateSet[1] += 1;
                break;
            case 'm-':
                if (dateSet[1] > 0) {
                    dateSet[1] -= 1;
                }
                if (dateSet[1] === 0) {
                    dateSet[1] = 12;
                }
                break;

            case 'd+':
                if (dateSet[2] < 99) dateSet[2] += 1;
                break;
            case 'd-':
                if (dateSet[2] > 0) {
                    dateSet[2] -= 1;
                }
                if (dateSet[2] === 0) {
                    dateSet[2] = 99;
                }
                break;

            case 'h+':
                if (dateSet[3] < 99) dateSet[3] += 1;
                break;
            case 'h-':
                if (dateSet[3] > 0) {
                    dateSet[3] -= 1;
                }
                if (dateSet[3] === 0) {
                    dateSet[3] = 99;
                }
                break;

            case 'min+':
                if (dateSet[4] < 59) dateSet[4] += 1;
                break;
            case 'min-':
                if (dateSet[4] > 0) {
                    dateSet[4] -= 1;
                }
                if (dateSet[4] === 0) {
                    dateSet[4] = 59;
                }
                break;

            case 's+':
                if (dateSet[5] < 59) dateSet[5] += 1;
                break;
            case 's-':
                if (dateSet[5] > 0) {
                    dateSet[5] -= 1;
                }
                if (dateSet[5] === 0) {
                    dateSet[5] = 59;
                }
                break;

            case 'set':
                let date = new Date(), now = new Date();
                date.setUTCFullYear(now.getUTCFullYear() + dateSet[0]);
                date.setUTCMonth(now.getUTCMonth() + dateSet[1]);
                date.setUTCDate(now.getUTCDate() + dateSet[2]);
                date.setUTCHours(now.getUTCHours() + dateSet[3]);
                date.setUTCMinutes(now.getUTCMinutes() + dateSet[4]);
                date.setUTCSeconds(now.getUTCSeconds() + dateSet[5]);

                if (deltaTimestampToArray(date.getTime(), new Date())[0] >= 100) {
                    $('div#stats').html(`Time must not exceed 100 years.`);
                    $('div#stats').fadeIn(300, () => {
                        setTimeout(() => {
                            $('div#stats').fadeOut(300);
                        }, 5000);
                    });
                    return;
                }

                let timeMin = new Date();
                timeMin.setUTCSeconds(timeMin.getUTCSeconds() + setMinSec)
                if (timeMin.getTime() > date.getTime()) {
                    $('div#stats').html(`Check you time. (Minimum = ${setMinSec}s)`);
                    $('div#stats').fadeIn(300, () => {
                        setTimeout(() => {
                            $('div#stats').fadeOut(300);
                        }, 5000);
                    });
                } else {
                    targetTime = date.getTime();
                    var evt = new CustomEvent('hideTool');
                    window.dispatchEvent(evt);
                    beginDateOp(host, date.getTime(), (d) => {
                        auto_update = true;
                        if (d === undefined) {
                            if (!firstSet) firstSet = true;
                        }
                    });
                }
                return;
            case 'toggle':
                var evt = new CustomEvent('toggleTool');
                window.dispatchEvent(evt);
        }
        var i = 0;
        dateSet.forEach(dateS => {
            timeElements[i].html((dateS > 9) ? dateS.toString() : `0${dateS}`);
            i += 1;
        });
    });

    // Screen update
    setInterval(() => {
        if (isToolOpen) return;

        if (targetTime !== 0 && targetTime >= new Date() && !$('div#time_set').is(':visible')) {
            render(deltaTimestampToArray(targetTime, new Date()), timeElements);
            auto_update = true;
        } else if (targetTime < new Date() && !$('div#time_set').is(':visible')) {
            render([0, 0, 0, 0, 0, 0], timeElements);
            var evt = new CustomEvent('showTool');
            window.dispatchEvent(evt);
            if (firstSet) {
                var evt = new CustomEvent('notification', {detail: 'Time\'s up!'});
                window.dispatchEvent(evt);
            }
            auto_update = false;
        }
    }, 100);

    setInterval(() => {
        if (auto_update) {
            beginDateOp(host, null, (newTime) => {
                if (newTime == null) {
                    if (targetTime < new Date()) {
                        var evt = new CustomEvent('showTool');
                        window.dispatchEvent(evt);
                    }
                    auto_update = false;
                    return;
                } else if (newTime === false) {
                    if (targetTime < new Date()) {
                        var evt = new CustomEvent('showTool');
                        window.dispatchEvent(evt);
                    }
                    auto_update = false;
                    return;
                } else if (newTime !== targetTime) {
                    var evt = new CustomEvent('notification', {detail: 'Time has updated!'});
                    window.dispatchEvent(evt);
                    targetTime = newTime;
                }
            });
        }
    }, updateInterval);

    window.addEventListener('notification', (e) => {
        $('audio').get(0).volume = 1;
        $('audio').get(0).play();
        new Notification('Hey! I\'m countdown.', {
            body: e.detail
        });
    });
}
