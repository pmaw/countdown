const { app, BrowserWindow } = require('electron');
const path = require('path')

var start = () => {
    let w = new BrowserWindow({
        title: 'Countdown',

        width: 800,
        height: 200,
        webPreferences: {
            nodeIntegration: true
        },

        icon: path.join(__dirname, 'ui/resources/icon.png')
    });

    w.loadFile('ui/index.html');
    w.removeMenu();
    w.resizable = false;
    //w.webContents.openDevTools();
}

app.allowRendererProcessReuse = false;
app.whenReady().then(start);